-- Answers to Exercise 6 here
DROP TABLE IF EXISTS dblab12_ex06_table;

CREATE TABLE IF NOT EXISTS dblab12_ex06_table (
  id       INT          NOT NULL AUTO_INCREMENT,
  title    VARCHAR(100) NOT NULL,
  director VARCHAR(100) NOT NULL,
  rate     INT          NOT NULL,
  loan     VARCHAR(100),
  PRIMARY KEY (id),
  FOREIGN KEY (loan) REFERENCES dblab12_ex03_table (name)
);

INSERT INTO dblab12_ex06_table (title, director, rate, loan) VALUES
  ("The Fate of the Furious", "F. Gary Gray", 6, 'Jane Campion'),
  ("Straight Outta Compton", "F. Gary Gray", 2, NULL),
  ("Star Wars: The Last Jedi", "Rian Johnson", 4, 'Anika Moa'),
  ("Guardians of the Galaxy Vol. 2 ", " James Gunn", 6, 'Apirana Turupa Ngata'),
  ("A Man Apart", "F. Gary Gray", 2, 'Lucy Lawless');

SELECT *
FROM dblab12_ex06_table;