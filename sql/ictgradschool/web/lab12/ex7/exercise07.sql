-- Answers to Exercise 7 here
DROP TABLE IF EXISTS dblab12_ex07_table;

CREATE TABLE IF NOT EXISTS dblab12_ex07_table (
  id int NOT NULL AUTO_INCREMENT,
  article VARCHAR(999) NOT NULL ,
  comment VARCHAR(8000),
  PRIMARY KEY (id),
  FOREIGN KEY (article) REFERENCES dblab12_ex04_table (title)
);

INSERT INTO dblab12_ex07_table ( article,comment) VALUES
  ('What is Lorem Ipsum?','nice'),
  ('What is Lorem Ipsum?','so-so'),
  ('Why do we use it?','OK'),
  ('Where can I get some?','Haha');

SELECT *
FROM dblab12_ex07_table;