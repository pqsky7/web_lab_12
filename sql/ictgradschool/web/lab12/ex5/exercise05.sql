-- Answers to Exercise 5 here
DROP TABLE IF EXISTS dblab12_ex05_table;

CREATE TABLE IF NOT EXISTS dblab12_ex05_table (
  username   VARCHAR(100) NOT NULL,
  first_name VARCHAR(100),
  last_name  VARCHAR(100),
  email      VARCHAR(100),
  PRIMARY KEY (username)
);

INSERT INTO dblab12_ex05_table (username, first_name, last_name, email) VALUES
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('programmer2', 'Peter', 'Green', 'peter@microsoft.com'),
  ('programmer3', 'Pete', 'Smith', 'pete@microsoft.com'),
  ('programmer4', 'James', 'Peterson', 'peterson@microsoft.com');

SELECT *
FROM dblab12_ex05_table;