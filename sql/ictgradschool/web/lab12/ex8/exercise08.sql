-- Answers to Exercise 8 here
DELETE FROM dblab12_ex05_table
WHERE first_name = 'bill';
SELECT * FROM dblab12_ex05_table;

ALTER TABLE dblab12_ex06_table
DROP COLUMN director;
SELECT * FROM dblab12_ex06_table;

DROP TABLE IF EXISTS dblab12_ex05_table;

CREATE TABLE IF NOT EXISTS dblab12_ex05_table (
    username   VARCHAR(100) NOT NULL,
    first_name VARCHAR(100),
    last_name  VARCHAR(100),
    email      VARCHAR(100),
    PRIMARY KEY (username)
);

INSERT INTO dblab12_ex05_table (username, first_name, last_name, email) VALUES
    ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
    ('programmer2', 'Peter', 'Green', 'peter@microsoft.com'),
    ('programmer3', 'Pete', 'Smith', 'pete@microsoft.com'),
    ('programmer4', 'James', 'Peterson', 'peterson@microsoft.com');

SELECT *
FROM dblab12_ex05_table;

DROP TABLE IF EXISTS dblab12_ex06_table;

CREATE TABLE IF NOT EXISTS dblab12_ex06_table (
    id       INT          NOT NULL AUTO_INCREMENT,
    title    VARCHAR(100) NOT NULL,
    director VARCHAR(100) NOT NULL,
    rate     INT          NOT NULL,
    loan     VARCHAR(100),
    PRIMARY KEY (id),
    FOREIGN KEY (loan) REFERENCES dblab12_ex03_table (name)
);

INSERT INTO dblab12_ex06_table (title, director, rate, loan) VALUES
    ("The Fate of the Furious", "F. Gary Gray", 6, 'Jane Campion'),
    ("Straight Outta Compton", "F. Gary Gray", 2, NULL),
    ("Star Wars: The Last Jedi", "Rian Johnson", 4, 'Anika Moa'),
    ("Guardians of the Galaxy Vol. 2 ", " James Gunn", 6, 'Apirana Turupa Ngata'),
    ("A Man Apart", "F. Gary Gray", 2, 'Lucy Lawless');

SELECT *
FROM dblab12_ex06_table;

update dblab12_ex05_table
    set email= 'hello@world.com'
where first_name = 'bill';

UPDATE dblab12_ex06_table
set rate = 10
WHERE id = 4;

UPDATE dblab12_ex05_table
set username = 'dkfjalfjalfj'
where first_name = 'bill';


