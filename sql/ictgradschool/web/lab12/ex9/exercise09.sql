-- Answers to Exercise 9 here
SELECT * FROM dblab12_ex03_table;
SELECT name,gender,year_born,joined FROM dblab12_ex03_table;
SELECT title from dblab12_ex04_table;
SELECT DISTINCT director from dblab12_ex06_table;
SELECT title from dblab12_ex06_table where rate <= 2;
SELECT username from dblab12_ex05_table ORDER BY username;
SELECT username from dblab12_ex05_table where first_name like 'Pete%';
SELECT username from dblab12_ex05_table where first_name like 'Pete%' OR last_name like 'Pete%';


/*DROP TABLE IF EXISTS dblab12_ex03_table_temp;
create TEMPORARY TABLE IF NOT EXISTS dblab12_ex03_table_temp
  SELECT * FROM dblab12_ex03_table;

ALTER TABLE dblab12_ex03_table_temp
    DROP COLUMN num_hires;*/